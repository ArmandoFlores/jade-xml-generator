# A jade based XML file generator

## How to use

- Install node
- Install jade ( npm install -g jade)
- create jade files to represent the XML data you need
- generate the files with: jade -E xml my-test.jade

*For more info please visit http://jade-lang.com/*

## 